import React from 'react';
import { configure, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-styled-components';
import Header from './Header';
import Title from '../../components/Title/Title';

configure({ adapter: new Adapter() });

describe('<Header />', () => {
  it('should contain a Title component', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.contains(<Title />));
  });
  it('should contain the text Brastlewark Town', () => {
    const text = 'Brastlewark Town';
    const wrapper = shallow(<Title text={text} />);
    expect(wrapper.text()).toEqual(text);
  });
  it('should set text alignment to center and font color to white', () => {
    const tree = renderer.create(<Title center white />).toJSON();
    expect(tree).toHaveStyleRule('text-align', 'center');
    expect(tree).toHaveStyleRule('color', 'white');
  });
});
