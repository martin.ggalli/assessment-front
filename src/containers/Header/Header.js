import React from 'react';
import styled from 'styled-components';
import Title from '../../components/Title/Title';

const StyledHeader = styled.header`
  height: 100px;
  width: 100%;
  background: #417505;
  display: table;
`;

const Header = () => {
  return (
    <StyledHeader>
      <Title center white text={'Brastlewark Town'} />
    </StyledHeader>
  );
};

export default Header;
