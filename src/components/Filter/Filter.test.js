import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-styled-components';
import Filter from './Filter';

configure({ adapter: new Adapter() });

describe('<Filter />', () => {
  it('should call onChange prop', () => {
    const onSearch = jest.fn();
    const event = {
      preventDefault() {},
      target: { value: 'the-value' },
    };
    const component = shallow(<Filter onTextChange={onSearch} />);
    component.find('#outlined-search').simulate('change', event);
    expect(onSearch).toBeCalledWith('the-value');
  });
  it('should call onChange prop with input value', () => {
    const onSearch = jest.fn();
    const component = mount(
      <Filter onTextChange={onSearch} value="custom value" />
    );
    component.find('input').simulate('change');
    expect(onSearch).toBeCalledWith('custom value');
  });
});
