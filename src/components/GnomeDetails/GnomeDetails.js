import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';
import Title from '../Title/Title';

const StyledPaper = styled(Paper)`
  padding: 15px;
`;

const StyledGnomeNameAndAvatar = styled.div`
  display: flex;
`;

const StyledGnomeAvatar = styled.div`
  margin-right: 15px;
  width: 50px;
  height: 50px;
  border-radius: 100%;
  background-image: url(${(props) =>
    props.thumbnail ? props.thumbnail : null});
  background-repeat: no-repeat;
  background-size: cover;
`;

const StyledGnomeDetails = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledListItem = styled.li`
  list-style: none;
  padding: 10px 0px;
`;

const SelectGnomeMessage = styled.p`
  text-align: center;
`;

const GnomeDetails = ({
  id,
  name,
  thumbnail,
  age,
  weight,
  height,
  hairColor,
  professions,
  friends,
  handleFriendClick,
}) => {
  let gnome = null;
  if (id || id === 0) {
    gnome = (
      <StyledPaper>
        <StyledGnomeNameAndAvatar>
          <StyledGnomeAvatar thumbnail={thumbnail} />
          <Title text={name} />
        </StyledGnomeNameAndAvatar>
        <StyledGnomeDetails>
          <div>
            <ul>
              <StyledListItem>
                Age: <strong>{age}</strong>
              </StyledListItem>
              <StyledListItem>
                Weight: <strong>{weight.toFixed(2)}</strong>
              </StyledListItem>
              <StyledListItem>
                Height: <strong>{height.toFixed(2)}</strong>
              </StyledListItem>
              <StyledListItem>
                Hair Color:{' '}
                <strong style={{ color: hairColor }}>{hairColor}</strong>
              </StyledListItem>
            </ul>
          </div>
          <div>
            <h3>Professions</h3>
            {professions.length > 0 ? (
              <ul>
                {professions.map((p) => {
                  return <StyledListItem key={p}>{p}</StyledListItem>;
                })}
              </ul>
            ) : (
              <p>{name} has no professions!</p>
            )}
          </div>
          <div>
            <h3>Friends</h3>
            {friends.length > 0 ? (
              <ul>
                {friends.map((f, index) => {
                  return (
                    <StyledListItem
                      onClick={() => handleFriendClick(name, index)}
                      key={f}
                    >
                      {f}
                    </StyledListItem>
                  );
                })}
              </ul>
            ) : (
              <p>{name} has no friends!</p>
            )}
          </div>
        </StyledGnomeDetails>
      </StyledPaper>
    );
  }

  return (
    <>
      {gnome ? (
        gnome
      ) : (
        <SelectGnomeMessage>
          Select a Gnome to show it's details.
        </SelectGnomeMessage>
      )}
    </>
  );
};

GnomeDetails.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  thumbnail: PropTypes.string,
  age: PropTypes.number,
  weight: PropTypes.number,
  height: PropTypes.number,
  hairColor: PropTypes.string,
  professions: PropTypes.array,
  friends: PropTypes.array,
  handleFriendClick: PropTypes.func,
};

export default GnomeDetails;
