import React from 'react';
import { configure, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-styled-components';
import Title from './Title';

configure({ adapter: new Adapter() });

describe('<Title />', () => {
  it('should render without crashing', () => {
    shallow(<Title />);
  });
  it('should render an h1 html tag', () => {
    const wrapper = shallow(<Title />);
    expect(wrapper.find('h1'));
  });
  it('should set the text align to center', () => {
    const tree = renderer.create(<Title center />).toJSON();
    expect(tree).toHaveStyleRule('text-align', 'center');
  });
  it('should set font color to white', () => {
    const tree = renderer.create(<Title white />).toJSON();
    expect(tree).toHaveStyleRule('color', 'white');
  });
  it('should set text alignment to center and font color to white', () => {
    const tree = renderer.create(<Title center white />).toJSON();
    expect(tree).toHaveStyleRule('text-align', 'center');
    expect(tree).toHaveStyleRule('color', 'white');
  });
});
