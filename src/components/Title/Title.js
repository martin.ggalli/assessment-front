import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledTitle = styled.h1`
  text-align: center;
  margin: 0;
  display: table-cell;
  vertical-align: middle;
  text-align: ${(props) => (props.center ? 'center' : 'left')};
  color: ${(props) => (props.white ? 'white' : 'black')};
`;

const Title = ({ text, center, white }) => {
  return (
    <StyledTitle center={center} white={white}>
      {text}
    </StyledTitle>
  );
};

Title.defaultProps = {
  text: '',
};

Title.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  algnment: PropTypes.string,
};

export default Title;
