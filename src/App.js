import React from 'react';
import styled from 'styled-components';
import Header from './containers/Header/Header';
import Gnomes from './containers/Gnomes/Gnomes';

const Main = styled.main`
  width: 98%;
  display: block;
  margin: 30px auto;
`;

function App() {
  return (
    <div className="App">
      <Header />
      <Main>
        <Gnomes />
      </Main>
    </div>
  );
}

export default App;
