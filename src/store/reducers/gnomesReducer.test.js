import gnomesReducer from './gnomesReducer';
import * as actionTypes from '../actions/actionTypes';

describe('Gnomes Reducer', () => {
  it('should return initial state', () => {
    expect(gnomesReducer(undefined, {})).toEqual({
      gnomes: [],
      loading: false,
      error: null,
    });
  });
  it('should set the loader when starting to fetch gnomes', () => {
    expect(
      gnomesReducer(
        {
          gnomes: [],
          loading: false,
          error: null,
        },
        {
          type: actionTypes.FETCH_GNOMES_START,
          loading: true,
          error: null,
        }
      )
    ).toEqual({
      gnomes: [],
      loading: true,
      error: null,
    });
  });
});
